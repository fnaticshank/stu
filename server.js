var express = require('express');
var bodyParser = require('body-parser');

// setup
var app = express();
var http = require('http').Server(app);

var port = process.env.PORT || 8080;

// grab POST requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
  next();
});

// Uncomment the following to start serving api
//var api = require('./src/api.js');
//app.use('/api', api);

// route all requests to Angular
app.get('*', function(req, res) {
  res.sendFile(__dirname + '/public/index.html');
})

http.listen(port, function() {
  console.log('Woot! Server is up and running! on port ' + port);
});
