Stu
-----------

Coming soon.

### Setup

* To being development run `npm install` first thing.
* Install the `compass` ruby gem for sass to css conversion.

```
$ gem install compass --version 0.12.6
```

* Add `export NODE_ENV=development` to your bashrc/zshrc file.
* Run `npm run build` to build files.
* Run `npm start` and you'll be live!

### Development

* Globally install nodemon by `npm install nodemon -g`
* Run `npm run build` (optional).
* Run `gulp watch` to watch, build js and css files.
* Run `npm run dev` and hack! :)
