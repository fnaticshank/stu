angular.module('stu')
.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/views/home.html'
      })
      .state('signin', {
        url: '/signin',
        templateUrl: 'app/views/signin.html',
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/views/signup.html',
      })
      .state('search-record', {
        url: '/search-record',
        templateUrl: 'app/views/record-search.html',
        controller: 'searchCtrl',
        controllerAs: 'vm'

      })
      .state('create-record', {
        url: '/create-record',
        templateUrl: 'app/views/record-create.html',
        controller: 'postCtrl',
        controllerAs: 'vm'
      })
      .state('modify-record', {
        url: '/modify-record',
        templateUrl: 'app/views/record-modify.html',
      })
  
  
  
    $locationProvider.html5Mode(true);
})
