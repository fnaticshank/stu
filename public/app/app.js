// include our routes, services, filters and controllers here
// Time to rock with dependency injection

//angular.module('stu', [require('angular-route')]);
angular.module('stu', [
  'ngRoute',
  'ui.router',
  'angular-loading-bar'
])
