angular.module('stu')
.controller('searchCtrl', function(records) {

  var vm = this;

  vm.records = records.list;

})

.controller('postCtrl', function(records) {

  var vm = this;
  vm.student = {
      name: '',
      age: '',
      course: '',
      email: '',
      is_active: ''
  }
  vm.flash = '';

  vm.save = function save() {
    records.add(vm.student);
    console.log('User created',vm.student);
    vm.flash = 'Successful'
  }
})
