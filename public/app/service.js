angular.module('stu')
.factory('records', function() {

  var records = {};

  records.list = [
    { name: 'Alice', age: '13', course: 'English', email: 'alice@mail.com', is_active: true },
    { name: 'Bob', age: '14', course: 'Maths', email: 'bob@mail.com', is_active: true },
    { name: 'Charlie', age: '15', course: 'Science', email: 'charlie@mail.com', is_active: true },
    { name: 'Damon', age: '16', course: 'History', email: 'delta@mail.com', is_active: true }
  ]
    
  records.add = function (record) {
    records.list.push({
      name: record.name,
      age: record.age,
      course: record.course,
      email: record.email,
      is_active: record.is_active
    });
    console.log(records.list);
  };

  return records;
})

