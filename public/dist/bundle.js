// include our routes, services, filters and controllers here
// Time to rock with dependency injection

//angular.module('stu', [require('angular-route')]);
angular.module('stu', [
  'ngRoute',
  'ui.router',
  'angular-loading-bar'
])

angular.module('stu')
.controller('searchCtrl', function(records) {

  var vm = this;

  vm.records = records.list;

})

.controller('postCtrl', function(records) {

  var vm = this;
  vm.student = {
      name: '',
      age: '',
      course: '',
      email: '',
      is_active: ''
  }
  vm.flash = '';

  vm.save = function save() {
    records.add(vm.student);
    console.log('User created',vm.student);
    vm.flash = 'Successful'
  }
})

angular.module('stu')
.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/views/home.html'
      })
      .state('signin', {
        url: '/signin',
        templateUrl: 'app/views/signin.html',
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/views/signup.html',
      })
      .state('search-record', {
        url: '/search-record',
        templateUrl: 'app/views/record-search.html',
        controller: 'searchCtrl',
        controllerAs: 'vm'

      })
      .state('create-record', {
        url: '/create-record',
        templateUrl: 'app/views/record-create.html',
        controller: 'postCtrl',
        controllerAs: 'vm'
      })
      .state('modify-record', {
        url: '/modify-record',
        templateUrl: 'app/views/record-modify.html',
      })
  
  
  
    $locationProvider.html5Mode(true);
})

angular.module('stu')
.factory('records', function() {

  var records = {};

  records.list = [
    { name: 'Alice', age: '13', course: 'English', email: 'alice@mail.com', is_active: true },
    { name: 'Bob', age: '14', course: 'Maths', email: 'bob@mail.com', is_active: true },
    { name: 'Charlie', age: '15', course: 'Science', email: 'charlie@mail.com', is_active: true },
    { name: 'Damon', age: '16', course: 'History', email: 'delta@mail.com', is_active: true }
  ]
    
  records.add = function (record) {
    records.list.push({
      name: record.name,
      age: record.age,
      course: record.course,
      email: record.email,
      is_active: record.is_active
    });
    console.log(records.list);
  };

  return records;
})

