var gulp = require('gulp');
var path = require('path');
var concat = require('gulp-concat');
var compass = require('gulp-compass');
var minifyCSS = require('gulp-minify-css');

gulp.task('style', function() {
  gulp.src('./public/assets/sass/**/*')
      .pipe(compass({
        project: path.join(__dirname, '/public'),
        css: 'dist',
        sass: 'assets/sass'
      }))
      .pipe(minifyCSS())
      .pipe(gulp.dest('dist'));
});

gulp.task('script', function () {
  gulp.src(['public/app/**/module.js', 'public/app/**/*.js'])
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest('public/dist'))
})

gulp.task('watch', ['script', 'style'], function () {
  gulp.watch('public/app/**/*.js', ['script'])
  gulp.watch('public/assets/sass/**/*.sass', ['style']);
})
